class ApplicationMailer < ActionMailer::Base
  default :from => "noanswer@flosscoach.com"
  layout 'mailer'
end
